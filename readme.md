### Projet Quiz - Typescript/DOM et Bootstrap


## Description
Le projet Quiz est un quiz interactif conçu pour tester les connaissances des utilisateurs. Il a été développé en utilisant TypeScript/DOM pour la logique du côté client et Bootstrap pour un design réactif.

## Fonctionnalités principales
- **Affichage des questions et choix de la réponse :** Les utilisateurs peuvent voir les questions posées avec des options de réponse.
- **Enchaînement de plusieurs questions :** Le quiz enchaîne plusieurs questions sans nécessiter un changement de page.
- **Décompte des points et affichage du score :** Les points sont comptabilisés en fonction des réponses correctes, et le score final est affiché à la fin du quiz.
- **Chronomètre :** Un timer intégré pour chronométrer la durée du quiz.
- **Responsive :** Le quiz est conçu pour s'adapter à différentes tailles d'écrans.

## Compétences mobilisées
- **Variables :** Utilisation de variables pour stocker des informations telles que les questions, les réponses et les points.
- **Conditions :** Utilisation de conditions pour évaluer si une réponse est correcte et pour afficher des éléments en fonction du déroulement du quiz.
- **Tableaux :** Stockage des questions et réponses dans des tableaux pour une gestion plus efficace.
- **Boucles :** Utilisation de boucles pour itérer à travers les questions et les options de réponse.
- **Fonctions :** Organisation du code en fonctions pour rendre le code plus modulaire et réutilisable.

## Technologies utilisées
- TypeScript/DOM
- Bootstrap

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/quiz.git`
2. Ouvrez le fichier `index.html` dans votre navigateur web.


## Auteurs
- Chems MEZIOU
