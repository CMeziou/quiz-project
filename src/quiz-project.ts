const questionArray: string[] = ["quel est le 1er homme de l'équipage de Gol D Roger ?", "luffy bat en 1 coup :", "Comment s'appelle le navire de gold roger ?", "(facile)barbe blanche a le pouvoir de :", "(Très facile !!!)Monkey D dragon est le chef de :", "quel est la mort la plus douloureuse pour luffy ?", 'qui est le père de ace ?', 'quel est le prelat que luffy bat en 1er ?']
const answersArray: string[][] = [
    ['Baggy Le Clone', 'Shanx Le Roux', 'Sylver Reighly'],
    ['Bellamy', 'Crocodile', 'Baggy'],
    ['Le Sunny', "L'Oro Jackson", 'Moby dick'],
    ['Detruire le monde', 'Changer le monde', 'Fracturer le monde'],
    ['Grand Line', 'La Marine', "L'armée Révolutionnaire"],
    ['la mort de Sabot', 'la mort de Ace', 'la mort du vogue merry'],
    ['barbe blanche', 'monkey-D garp', 'Gold Roger'],
    ['shura', 'ener', 'satori'],
];
let goodAnswerArray: string[] = ['Sylver Reighly', 'Bellamy', "L'Oro Jackson", 'Detruire le monde', "L'armée Révolutionnaire", 'la mort de Ace', 'Gold Roger', 'ener'];
const questions = document.querySelector<HTMLElement>('h3');
const reponse = document.querySelectorAll('.btn');
const answ1 = document.querySelector<HTMLButtonElement>('.answers1');
const answ2 = document.querySelector<HTMLButtonElement>('.answers2');
const answ3 = document.querySelector<HTMLButtonElement>('.answers3');
let score = document.querySelector<HTMLElement>('.score');
let scoreClac: number = 0;
let index = 0;
let timmer:any = document.querySelector('.timmer p');
let time;
let second = 90;
function timeDown() {
    time = setInterval(() => {
        timmer.innerHTML = second;
        second--;
        if (second < 0 || index == questionArray.length) {
            timmer.innerHTML = "End";
            if(questions)
            questions.innerHTML = "End of game" + "<br>" + "<br>" + "Your score is: " + `${scoreClac} / ${questionArray.length}`;
            answ1?.remove();
            answ2?.remove();
            answ3?.remove();

        }
    }, 1000)
}
timeDown();
function removeAnswers(){
    answ1?.remove();
    answ2?.remove();
    answ3?.remove();
}
function changeQuestion(){
    if (questions) {
        questions.innerHTML = questionArray[index];
    }
}
function changeReponse(){
    if (answ1 && answ2 && answ3) {
        answ1.innerHTML = answersArray[index][2];
        answ2.innerHTML = answersArray[index][1];
        answ3.innerHTML = answersArray[index][0];
    }
}

if (reponse) {
    for (const rep of reponse) {
        rep.addEventListener('click', (event) => {
            event.preventDefault();

            if (rep.textContent == goodAnswerArray[index]) {
                if (score) {
                    scoreClac++;
                    score.innerHTML = `${scoreClac}`
                }
                setTimeout(() => {
                    if (questionArray.length > index) {
                        changeQuestion();
                        changeReponse();
                        rep.classList.remove('ValidReponse');
                    } else {
                        removeAnswers()
                        if (questions) {
                            questions.innerHTML = "End of game" + "<br>" + "<br>" + "Your score is: " + `${scoreClac} / ${questionArray.length}`;
                        }
                    }
                }, 1000);
                rep.classList.add('ValidReponse');
            } else {
                setTimeout(() => {
                    if (questionArray.length > index) {
                        changeQuestion();
                        changeReponse();
                        rep.classList.remove('NotValidReponse');
                    } else {
                        removeAnswers()
                        if (questions) {
                            questions.innerHTML = "End of game" + "<br>" + "<br>" + "Your score is: " + `${scoreClac} / ${questionArray.length}`;
                        }
                    }
                }, 1000);
                rep.classList.add('NotValidReponse');
            }
            
            index++;
        })
    }
}
changeQuestion();
changeReponse();
